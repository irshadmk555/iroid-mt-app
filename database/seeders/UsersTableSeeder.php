<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (!User::where('id', 1)->exists()) {
            $user = new User();
            $user->id = 1;
            $user->name = "Admin";
            $user->email = "admin@admin.com";
            $user->password = bcrypt('password');
            $user->created_at = Carbon::now();
            $user->save();
        }
    }
}
