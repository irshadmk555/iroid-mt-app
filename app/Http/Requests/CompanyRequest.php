<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules= [
            "name" => 'required|max:500',
            "description" => "string|required",
            "mobile" => "string|required",
            "annual_turnover" => "numeric|required"
        ];
        if ($this->isMethod('post')) {
            $rules['logo'] = 'required|mimes:jpeg,png,jpg|max:2048';
        }

        return $rules;
    }
    public function messages()
    {
        return [
            'logo.required' => 'The image is required.',
            'logo.image' => 'The file must be an image.',
            'logo.mimes' => 'The image must be a file of type: jpeg, png, jpg.',
            'logo.max' => 'The image may not be greater than 2048 kilobytes.',
        ];
    }
}
