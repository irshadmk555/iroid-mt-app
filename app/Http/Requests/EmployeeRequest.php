<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules= [
            "name" => 'required|max:500',
            "company_id" => "integer|required",
            "email" => "string|required",
            "phone" => "string|required",
            "join_date" => "date|required",
        ];
        if ($this->isMethod('post')) {
            $rules['image'] = 'required|mimes:jpeg,png,jpg|max:2048';
        }

        return $rules;
    }
    public function messages()
    {
        return [
            'image.required' => 'The image is required.',
            'image.image' => 'The file must be an image.',
            'image.mimes' => 'The image must be a file of type: jpeg, png, jpg.',
            'image.max' => 'The image may not be greater than 2048 kilobytes.',
        ];
    }
}
