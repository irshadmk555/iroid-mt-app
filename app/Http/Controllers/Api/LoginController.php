<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(AuthRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user || !Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $token = $user->createToken('token-name')->plainTextToken;

        return response()->json([
            'id' => $user->id,
            'is_two_factor_auth_enabled' => false,
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::now()->addMinutes(config('sanctum.expiration'))->toDateTimeString()
        ]);
    }
}
