<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $companies = Company::get();
        $employees =  Employee::with(['createdBy','updatedBy','company'])->get();
        return view('employees',compact(['companies','employees']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(EmployeeRequest $request)
    {

        $employee = new Employee();
            $employee->name = $request->name;
            $employee->company_id = $request->company_id;
            $employee->email = $request->email;
            $employee->phone = $request->phone;
            $employee->join_date = $request->join_date;

            $employee->created_by = Auth::user()->id;
            if ($request->hasFile('image')) {
                $path = $request->file('image')->store('employee','public');
                $employee->image=$path;
             }
             DB::transaction(function () use ($employee) {
                $employee->save();
             });

             return redirect()->route('employees.index')->with('success', 'employee Added successfully..!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {


        $employee =  Employee::with(['createdBy','updatedBy','company'])->find($id);
        return view('employe_detaile',compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $companies = Company::get();
        $employee =  Employee::find($id);
        return view('edit_employees',compact(['companies','employee']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EmployeeRequest $request, string $id)
    {
        $employee = Employee::find($id);
        $employee->name = $request->name;
        $employee->company_id = $request->company_id;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->join_date = $request->join_date;

        $employee->created_by = Auth::user()->id;
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('employee','public');
            $employee->image=$path;
         }
         DB::transaction(function () use ($employee) {
            $employee->save();
         });

         return redirect()->route('employees.index')->with('success', 'employee Added successfully..!');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $employee = Employee::find($id);
        DB::transaction(function () use ($employee) {
            $employee->delete();
        });
        return redirect()->route('employees.index')->with('success', 'Company deleted successfully..!');
    }
}
