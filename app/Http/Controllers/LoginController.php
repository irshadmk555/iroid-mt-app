<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return view('login');
    }
    /**
     * login
     *
     * @param  mixed $request
     * @return void
     */
    public function login(AuthRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (!Auth::attempt(['email' => $request->email,'password' => $request->password])) {
             return redirect()->back()->withInput()->withErrors(['password' => 'Invalid password']);
            }
            return redirect()->intended(route('dashboard'));
        }
        return redirect()->back()->withInput()->withErrors(['email' => 'Invalid email']);
    }
    /**
     * logout
     *
     * @return void
     */
    public function logout()
    {
        Auth::logout();
        return redirect()->intended( route('login'));
    }
}
