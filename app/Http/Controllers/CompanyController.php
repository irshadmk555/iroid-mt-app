<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $companies = Company::with(['createdBy','updatedBy'])->get();
        return view('companies',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CompanyRequest $request)
    {
            $company = new Company();
            $company->name = $request->name;
            $company->description = $request->description;
            $company->mobile = $request->mobile;
            $company->annual_turnover = $request->annual_turnover;
            $company->created_by = Auth::user()->id;
            if ($request->hasFile('logo')) {
                $path = $request->file('logo')->store('company','public');
                $company->logo=$path;
             }
             DB::transaction(function () use ($company) {
                $company->save();
             });

             return response()->route('companies.index')->with('success', 'Company Added successfully..!');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $company = Company::with(['createdBy','updatedBy'])->find($id);
        return view('company_detaile',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $company = Company::find($id);
        return view('edit_companies',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CompanyRequest $request, string $id)
    {
        $company = Company::find($id);
        $company->name = $request->name;
        $company->description = $request->description;
        $company->mobile = $request->mobile;
        $company->annual_turnover = $request->annual_turnover;
        $company->updated_by = Auth::user()->id;
        if ($request->hasFile('logo')) {
            $path = $request->file('logo')->store('company','public');
            $company->logo=$path;
         }
         DB::transaction(function () use ($company) {
            $company->save();
         });

         return redirect()->route('companies.show',['company' => $company->id])->with('success', 'Company updated successfully..!');

        }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $company = Company::find($id);
        DB::transaction(function () use ($company) {
            $company->employee()->delete();
            $company->delete();
        });
        return redirect()->route('companies.index')->with('success', 'Company deleted successfully..!');
    }
}
