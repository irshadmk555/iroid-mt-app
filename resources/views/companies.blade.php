@extends('layout.master')
@section('title','Dashboard')
@section('top_scripts')


<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/2.0.7/css/dataTables.bootstrap5.css" rel="stylesheet" />
@endsection
@section('style')
@endsection
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4" style="margin-left: 220px;">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Companies</h1>
      <div class="btn-toolbar mb-2 mb-md-0">
      </div>
    </div>
  {{-- content --}}
  <div class="main-body">
    <div class="container">
     <section class="content">
      <div class="container-fluid">
       <div class="row">
        <div class="col-12">
         <div class="card">
          <div class="card-header">
           <button type="button" data-bs-toggle="modal" data-bs-target="#company" class="btn btn-block btn-outline-info btn-sm col-2">Add Company</button>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
           {{-- table --}}
           <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>logo</th>
                    <th>Description</th>
                    <th>Mobile</th>
                    <th>Annual Turnover</th>
                    <th>Created By</th>
                    <th>Created At</th>
                    <th>view</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($companies as $company)


                <tr>
                    <td>{{$company->name}}</td>
                    <td>
                        <div class="col-3">  <img src="{{ asset('storage/' .$company->logo) }}" alt="logo" width="100%"></div>
                    </td>
                    <td>{{$company->description}}</td>
                    <td>{{$company->mobile}}</td>
                    <td>{{$company->annual_turnover}}</td>
                    <td>{{$company['createdBy']->name}}</td>
                    <td>{{\Carbon\Carbon::parse($company->created_at)->format('j-F-Y ,H:i a')}}</td>
                <td><a href="{{route('companies.show', ['company' => $company->id])}}" class="btn btn-outline-primary btn-xs"><i data-feather="eye"></i></a></td>
                </tr>

                @endforeach
            </tbody>
        </table>
           {{-- /table --}}
          </div>
          <!-- /.card-body -->
         </div>
         <!-- /.card -->
        </div>
       </div>
      </div>
     </section>
    </div>
   </div>
  </main>

  <!-- Modal -->
<div class="modal fade" id="company" tabindex="-1" aria-labelledby="companyLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
     <div class="modal-content">
      <div class="modal-header">
       <h5 class="modal-title" id="exampleModalLabel">Create Company</h5>
       <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
       </button>
      </div>

      <form method="post" action="{{route('companies.store')}}" enctype="multipart/form-data">
       @csrf
       <div class="modal-body">
        <div class="card-body">
         <div class="form-group">
          <label for="name">Name<span class="m-l-5 text-danger"> *</span></label>
          <input type="text" class="form-control" id="" name="name" value="" placeholder="Enter Company Name" />
          <span class="text-danger error-text name_error"></span>
         </div>
         <div class="form-group">
          <label for="description" class="form-label">Description<span class="m-l-5 text-danger"> *</span></label>
          <textarea name="description" class="form-control"  rows="5">{{ old('description') }}</textarea>
          <span class="text-danger error-text Email_error"></span>
         </div>
         <div class="form-group">
          <label for="">Contact Number<span class="m-l-5 text-danger"> *</span></label>
          <input type="text" class="form-control" name="mobile" value="" id="" placeholder="Enter Contact Number" />
         </div>
         <div class="form-group">
            <label for="name">Annul TurnOver<span class="m-l-5 text-danger"> *</span></label>
            <input type="text" class="form-control" id="" name="annual_turnover" value="" />
            <span class="text-danger error-text name_error"></span>
           </div>
         <div class="form-group">
          <label for="exampleInputFile">Logo<span class="m-l-5 text-danger"> *</span></label>
          <div class="input-group">
           <div class="custom-file">
            <input type="file" class="custom-file-input" name="logo" id="logo" />
           </div>
          </div>
          <span class="text-danger error-text Logo_error"></span>
         </div>
        </div>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
       </div>
      </form>
     </div>
    </div>
   </div>
@endsection
@section('bottom_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.datatables.net/2.0.7/js/dataTables.js"></script>
<script src="https://cdn.datatables.net/2.0.7/js/dataTables.bootstrap5.js"></script>
<script>
    new DataTable('#example')
</script>
;
@endsection

