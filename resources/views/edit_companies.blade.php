@extends('layout.master')
@section('title','Dashboard')
@section('top_scripts')
@endsection
@section('style')
@endsection
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4" style="margin-left: 220px;">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Dashboard</h1>
      <div class="btn-toolbar mb-2 mb-md-0">
      </div>
    </div>
  {{-- content --}}
  <div class="row">
    <form method="put" action="{{route('companies.update', ['company' => $company->id])}}" enctype="multipart/form-data">

        @csrf
        <div class="modal-body">
         <div class="card-body">
          <div class="form-group">
           <label for="name">Name<span class="m-l-5 text-danger"> *</span></label>
           <input type="text" class="form-control" id="" name="name" value="{{$company->name}}" placeholder="Enter Company Name" />
           <span class="text-danger error-text name_error"></span>
          </div>
          <div class="form-group">
           <label for="description" class="form-label">Description<span class="m-l-5 text-danger"> *</span></label>
           <textarea name="description" class="form-control" value="{{$company->description}}"  rows="5">{{$company->description}}</textarea>
           <span class="text-danger error-text Email_error"></span>
          </div>
          <div class="form-group">
           <label for="">Contact Number<span class="m-l-5 text-danger"> *</span></label>
           <input type="text" class="form-control" name="mobile" value="{{$company->mobile}}" id="" placeholder="Enter Contact Number" />
          </div>
          <div class="form-group">
             <label for="name">Annul TurnOver<span class="m-l-5 text-danger"> *</span></label>
             <input type="text" class="form-control" id="" value="{{$company->annual_turnover}}"  name="annual_turnover"  />
             <span class="text-danger error-text name_error"></span>
            </div>
          <div class="form-group">
           <label for="exampleInputFile">Logo<span class="m-l-5 text-danger"> *</span></label>
           <div class="input-group">
            <div class="custom-file">
             <input type="file" class="custom-file-input" name="logo" id="logo" />
            </div>
           </div>
           <span class="text-danger error-text Logo_error"></span>
          </div>
         </div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-primary">Update</button>
        </div>
       </form>
  </div>
  </main>
@endsection
@section('bottom_scripts')
@endsection

