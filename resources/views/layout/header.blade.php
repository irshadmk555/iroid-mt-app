<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{route('dashboard')}}">IROID TEST</a>
    <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
    <ul class="navbar-nav px-3">
      <li class="nav-item text-nowrap">
        {{-- <a class="nav-link" href="#"></a> --}}
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <a href="route('logout')" class="nav-link"
                    onclick="event.preventDefault();
                                this.closest('form').submit();"><i class="me-2 icon-md" data-feather="log-out"></i>
                {{ __('Log Out') }}
                </a>
        </form>
      </li>
    </ul>
  </nav>
