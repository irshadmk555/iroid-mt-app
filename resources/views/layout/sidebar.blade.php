<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link active" href="{{route('dashboard')}}">
            <span data-feather="home"></span>
            Dashboard <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('companies.index')}}">
            <span data-feather="plus"></span>
            Companies
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('employees.index')}}">
            <span data-feather="users"></span>
            Employees
          </a>
        </li>
      </ul>
    </div>
  </nav>
