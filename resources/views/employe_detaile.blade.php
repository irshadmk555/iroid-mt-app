@extends('layout.master')
@section('title','Dashboard')
@section('top_scripts')
@endsection
@section('style')
@endsection
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4" style="margin-left: 220px;">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Employee </h1>
      <div class="btn-toolbar mb-2 mb-md-0">
      </div>
    </div>
  {{-- content --}}
  <section class="content">
    <div class="card card-primary card-outline">
        <div class="card-body box-profile">
            <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" width="30%" src="{{ asset('storage/' .$employee->image) }}" alt="User profile picture">

            </div>
            <h3 class="profile-username text-center">{{$employee->name}}</h3>
            <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                    <b>Company</b> <a class="float-right">{{$employee['company']->name}}</a>
                </li>
                <li class="list-group-item">
                    <b>Contact Number</b> <a class="float-right">{{$employee->phone}}</a>
                </li>
                <li class="list-group-item">
                    <b>email</b> <a class="float-right">{{$employee->email}}</a>
                </li>
                <li class="list-group-item">
                    <b>Created By</b> <a class="float-right">{{$employee['createdBy']->name}}</a>
                </li>
                <li class="list-group-item">
                    <b>Created At</b> <a class="float-right">{{\Carbon\Carbon::parse($employee->created_at)->format('j-F-Y ,H:i a')}}</a>
                </li>

            </ul>

            <a  class="btn btn-primary btn-block col-2" href="{{route('employees.edit', ['employee' => $employee->id])}}" ><b>Edit</b></a>
            <form method="post" action="{{route('employees.destroy', ['employee' => $employee->id])}}">
               @method('delete')
                @csrf
                <button type="submit"  class="btn btn-danger btn-block col-2">
                    <b>Delete</b>
            </button>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
</section>
  </main>
@endsection
@section('bottom_scripts')
@endsection

