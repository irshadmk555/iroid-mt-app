@extends('layout.master')
@section('title','Dashboard')
@section('top_scripts')
@endsection
@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/2.0.7/css/dataTables.bootstrap5.css" rel="stylesheet" />
@endsection
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4" style="margin-left: 220px;">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Employees</h1>
      <div class="btn-toolbar mb-2 mb-md-0">
      </div>
    </div>
  {{-- content --}}
  <div class="main-body">
    <div class="container">
     <section class="content">
      <div class="container-fluid">
       <div class="row">
        <div class="col-12">
         <div class="card">
          <div class="card-header">
           <button type="button" data-bs-toggle="modal" data-bs-target="#employees" class="btn btn-block btn-outline-info btn-sm col-2">Add Employee</button>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
           {{-- table --}}
           <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>image</th>
                    <th>company</th>
                    <th>Mobile</th>
                    <th>email</th>
                    <th>Join Date</th>
                    <th>Created By</th>
                    <th>Created At</th>
                    <th>view</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($employees as $employee)


                <tr>
                    <td>{{$employee->name}}</td>
                    <td>
                        <div class="col-3">  <img src="{{ asset('storage/' .$employee->image) }}" alt="image" width="100%"></div>
                    </td>
                    <td>{{$employee['company']->name}}</td>
                    <td>{{$employee->phone}}</td>
                    <td>{{$employee->email}}</td>
                    <td>{{\Carbon\Carbon::parse($employee->join_date)->format('j-F-Y ')}}</td>
                    <td>{{$employee['createdBy']->name}}</td>
                    <td>{{\Carbon\Carbon::parse($employee->created_at)->format('j-F-Y ,H:i a')}}</td>
                <td><a href="{{route('employees.show', ['employee' => $employee->id])}}" class="btn btn-outline-primary btn-xs"><i data-feather="eye"></i></a></td>
                </tr>

                @endforeach
            </tbody>
        </table>
          </div>
          <!-- /.card-body -->
         </div>
         <!-- /.card -->
        </div>
       </div>
      </div>
     </section>
    </div>
   </div>
  </main>
@endsection
<!-- Modal -->
<div class="modal fade" id="employees" tabindex="-1" aria-labelledby="employeeLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Employee</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="EmployeeForm" method="post" action="{{route('employees.store')}}" enctype="multipart/form-data">
              @csrf
          <div class="card-body ">
            <div class="row">
            <div class="form-group col-6 inputname" >
              <label for="name">Name<span class="m-l-5 text-danger"> *</span></label>
              <input type="text" class="form-control" id="" name="name" value="" >
              <span class="text-danger error-text name_error"></span>
            </div>

            <div class="form-group col-6 inputname">
              <label for="name">Join Date<span class="m-l-5 text-danger"> *</span></label>
              <input type="date" class="form-control" id="" name="join_date" value="" >
              <span class="text-danger error-text LastName_error"></span>
            </div>

            <div class="form-group col-6 inputname">
              <label for="Email">Email address<span class="m-l-5 text-danger"> *</span></label>
              <input type="email" class="form-control" id="" name="email" placeholder="Enter email">
              <span class="text-danger error-text email_error"></span>
            </div>
            <div class="form-group col-6 inputname">
              <label for="indusrty">Phone</label>
              <input type="text" class="form-control" name="phone" value="" id="" >
              <span class="text-danger error-text phone_error"></span>
            </div>
            </div>
            <div class="form-group">
              <label for="">Company<span class="m-l-5 text-danger"> *</span></label>
              <select class="form-control" name="company_id" id="">
                @foreach($companies as $company)
                <option value="{{$company->id}}">{{$company->name}}</option>
                @endforeach

              </select>
              <span class="text-danger error-text company_id_error"></span>
            </div>

            <div class="form-group">
                <label for="exampleInputFile">Image<span class="m-l-5 text-danger"> *</span></label>
                <div class="input-group">
                 <div class="custom-file">
                  <input type="file" class="custom-file-input" name="image" id="logo" />
                 </div>
                </div>
                <span class="text-danger error-text image_error"></span>
               </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
  </form>
      </div>
    </div>
  </div>
@section('bottom_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.datatables.net/2.0.7/js/dataTables.js"></script>
<script src="https://cdn.datatables.net/2.0.7/js/dataTables.bootstrap5.js"></script>
<script>
    new DataTable('#example')
</script>
@endsection

