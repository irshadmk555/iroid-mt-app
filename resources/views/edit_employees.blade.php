@extends('layout.master')
@section('title','Dashboard')
@section('top_scripts')
@endsection
@section('style')
@endsection
@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4" style="margin-left: 220px;">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Dashboard</h1>
      <div class="btn-toolbar mb-2 mb-md-0">
      </div>
    </div>
  {{-- content --}}
  <div class="row">
    <form  method="post" action="{{route('employees.update', ['employee' => $employee->id])}}" enctype="multipart/form-data">
        @method('put')
        @csrf
    <div class="card-body ">
      <div class="row">
      <div class="form-group col-6 inputname" >
        <label for="name">Name<span class="m-l-5 text-danger"> *</span></label>
        <input type="text" class="form-control" id="" name="name" value="{{$employee->name}}" >
        <span class="text-danger error-text name_error"></span>
      </div>

      <div class="form-group col-6 inputname">
        <label for="name">Join Date<span class="m-l-5 text-danger"> *</span></label>
        <input type="date" class="form-control" id="" name="join_date" value="{{$employee->join_date}}" >
        <span class="text-danger error-text LastName_error"></span>
      </div>

      <div class="form-group col-6 inputname">
        <label for="Email">Email address<span class="m-l-5 text-danger"> *</span></label>
        <input type="email" class="form-control" id="" value="{{$employee->email}}" name="email" placeholder="Enter email">
        <span class="text-danger error-text email_error"></span>
      </div>
      <div class="form-group col-6 inputname">
        <label for="indusrty">Phone</label>
        <input type="text" class="form-control" name="phone" value="{{$employee->phone}}" id="" >
        <span class="text-danger error-text phone_error"></span>
      </div>
      </div>
      <div class="form-group">
        <label for="">Company<span class="m-l-5 text-danger"> *</span></label>
        <select class="form-control" name="company_id" id="">
          @foreach($companies as $company)
          <option value="{{$company->id}}" {{ old('company_id',$employee->company_id) == $company->id ? 'selected' : ''}}>{{$company->name}}</option>

          @endforeach

        </select>
        <span class="text-danger error-text company_id_error"></span>
      </div>

      <div class="form-group">
          <label for="exampleInputFile">Image<span class="m-l-5 text-danger"> *</span></label>
          <div class="input-group">
           <div class="custom-file">
            <input type="file" class="custom-file-input" name="image" id="logo" />
           </div>
          </div>
          <span class="text-danger error-text image_error"></span>
         </div>

    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">update</button>
  </div>
</form>
       </form>
  </div>
  </main>
@endsection
@section('bottom_scripts')
@endsection

